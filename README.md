# Moderation State Sync

Sync Content Moderation state between entity languages based on conditions.

## Example use case

If the source language reaches the `unpublished` state and it should
be the case for all other languages, so e.g. it stays consistent with a
Simple Sitemap inclusion per node override, that is synced.

## Configuration

- Enable the sync for a Workflow, under 'Workflow settings', check 'Enable moderation state sync'
- Then enable for the desired states: check 'Enable moderation state sync'
