<?php

namespace Drupal\moderation_state_sync;

use Drupal\Core\Entity\EntityInterface;
use Drupal\workflows\Entity\Workflow;

/**
 * Interface ModerationStateSyncInterface.
 */
interface ModerationStateSyncInterface {

  /**
   * Checks if a State for a given Workflow is to sync.
   *
   * @param \Drupal\workflows\Entity\Workflow $workflow
   * @param string $state_id
   *
   * @return bool
   */
  public function isStateToSync(Workflow $workflow, $state_id);

  /**
   * Syncs a content moderation state if the source language reaches it.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return void
   */
  public function syncEntityLanguagesState(EntityInterface $entity);

}
