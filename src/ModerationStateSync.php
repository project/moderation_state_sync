<?php

namespace Drupal\moderation_state_sync;

use Drupal\content_moderation\Entity\ContentModerationStateInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\workflows\Entity\Workflow;

/**
 * Class ModerationStateSync.
 */
class ModerationStateSync implements ModerationStateSyncInterface {

  use StringTranslationTrait;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\Core\Logger\LoggerChannelInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $loggerChannel;

  /**
   * Constructs a new ModerationStateSync object.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    MessengerInterface $messenger,
    LoggerChannelInterface $logger_channel
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
    $this->loggerChannel = $logger_channel;
  }

  /**
   * {@inheritDoc}.
   */
  public function isStateToSync(Workflow $workflow, $state_id) {
    $thirdPartySettings = $workflow->getThirdPartySettings('moderation_state_sync');
    return
      is_array($thirdPartySettings) &&
      array_key_exists('enable_moderation_state_workflow_sync', $thirdPartySettings) &&
      $thirdPartySettings['enable_moderation_state_workflow_sync'] &&
      array_key_exists('enabled_moderation_states_language_sync', $thirdPartySettings) &&
      is_array($thirdPartySettings['enabled_moderation_states_language_sync']) &&
      in_array($state_id, $thirdPartySettings['enabled_moderation_states_language_sync']);
  }

  /**
   * {@inheritDoc}.
   */
  public function syncEntityLanguagesState(EntityInterface $entity) {
    if (
      $entity instanceof ContentModerationStateInterface &&
      !$entity->get('workflow')->isEmpty() &&
      !$entity->get('langcode')->isEmpty() &&
      !$entity->get('content_entity_type_id')->isEmpty() &&
      !$entity->get('content_entity_id')->isEmpty() &&
      !$entity->get('moderation_state')->isEmpty()
    ) {
      $workflows = $entity->get('workflow')->referencedEntities();
      $workflow = reset($workflows);
      $languageId = $entity->get('langcode')->value;
      $contentEntityType = $entity->get('content_entity_type_id')->value;
      $contentEntityId = $entity->get('content_entity_id')->value;
      $moderationStateId = $entity->get('moderation_state')->value;
      if ($this->isStateToSync($workflow, $moderationStateId)) {
        try {
          $entityStorage = $this->entityTypeManager->getStorage($contentEntityType);
          $entity = $entityStorage->load($contentEntityId);
          if ($entity instanceof TranslatableInterface) {
            $originalLanguageId = $entity->language()->getId();
            if ($languageId === $originalLanguageId) {
              $translationLanguages = $entity->getTranslationLanguages(FALSE);
              $syncedLanguages = [];
              foreach ($translationLanguages as $translationLanguage) {
                $translatedEntity = $entity->getTranslation($translationLanguage->getId());
                $translatedEntity->set('moderation_state', $moderationStateId);
                $translatedEntity->save();
                $syncedLanguages[] = $translationLanguage->getName();
              }
              if (!empty($syncedLanguages)) {
                $this->messenger->addStatus($this->t('The moderation state <em>@state</em> has been synced with the <em>@translations</em> translations.', [
                  '@translations' => implode(', ', $syncedLanguages),
                  '@state' => $moderationStateId,
                ]));
              }
            }
          }
        }
        catch (\Exception $exception) {
          $this->messenger->addError($exception->getMessage());
          $this->loggerChannel->error($exception->getMessage());
        }
      }
    }
  }

}
